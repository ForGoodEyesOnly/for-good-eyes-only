# For Good Eyes Only Licence

![Fake News – License your code For Good Eyes Only to prevent misuse! (Background: Donald Trump)](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/assets/promo/promo-fakenews-750.png)

With this licence, software developers can make their programs available to others free of charge and without ignoring their own responsibility, because the licence conditions force licensees to behave in an ethical manner. Specifically, this Licence’s purpose is promoting and supporting a sustainable and dignified world by attaching conditions of justice and morality to the use and re-distribution of licensed works.

Therefore, the For Good Eyes Only Licence may be considered a Free Ethical Licence for [Ethical Source Software](https://ethicalsource.dev/principles).

## Preamble

Conscious of his/her responsibility before God and humankind, impelled by the will to serve the peace of the world, the Licensor, by virtue of his/her authorship rights, has placed his/her work under this For Good Eyes Only Licence. The human dignity shall be inviolable. Respecting it is the obligation of all power of the Licensee and the Licensor. The Licensee and the Licensor thus profess inviolable and inalienable human rights as the foundation of every human community, of peace and of justice in the world.

## Licence Summary

[![summary](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/assets/summary/Summary%20v0.2%20750px.png)](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/v0.2/Summary%20v0.2.pdf)

## FAQs

Please see [FAQ.md](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/src/branch/master/FAQ.md) for frequently asked questions.

## Contributors

Pixelcode sincerely thanks @twann (main developer of @tblock) for converting the licence text of version 0.1 into [.md](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/src/branch/master/v0.1/For%20Good%20Eyes%20Only%20Licence%20v0.1.md) and [.txt](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/src/branch/master/v0.1/For%20Good%20Eyes%20Only%20Licence%20v0.1.txt) format.

## Logo

|Logo|Flag|
|----|----|
|![logo](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/commit/342919c4507e0832d20501c4fff422b12119621b/assets/logo/logo-500.png) | ![banner](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/commit/342919c4507e0832d20501c4fff422b12119621b/assets/banner/Banner%20250.png) |

## Licence

You may use this licence, including its name, text, summary, icons, logos and banners, according to the [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/) licence, provided that, in case you change the licence text, you make its name, summary, icons, logos and banners distinguishable from the original.

Background photo of “Mass Surveillance” promo banner: &copy; [Pexels](https://pixabay.com/users/pexels-2286921/) on [Pixabay](https://pixabay.com/photos/building-cctv-door-female-ladies-1839464), [Pixabay Licence](https://pixabay.com/service/license/)

Background photo of “Fake News” promo banner: &copy; [Gerd Altmann](https://pixabay.com/users/geralt-9301) on [Pixabay](https://pixabay.com/photos/trump-president-usa-america-flag-2546104/), [Pixabay Licence](https://pixabay.com/service/license/)

Background photo of “Climate Change” promo banner: &copy; U.S. Department of Agriculture on [Flickr](https://www.flickr.com/photos/usdagov/9665091714/in/photolist-KtMjQH-Lg9bSL-KZ4K3G-oZPLW9-Lg8yJU-JBqPGf-ffmhft-nRY7mK-HJj89N-LqsK5K-HJiZgU-LqsJYn-Lg9bVw-LnwBru-Jvs7RL-ffmkhi-Lg9bPE-KnRFBA-LqsJZp-ffmk98-nzMpVB-ffmkeZ-nzLsmT-ffmh2k-nzMpQX-Lg8LDA-fCfjzp-ffAxYE-oZPRVX-JBryby-JBrEP3-JBrpZU-xAy2xo-phgZyu-oZP6Dy-JBqPFd-JEugia-fJ583j-KtvXoA-GjLhZu-ggvJFP-yUyxZh-ynjXPJ-HJf2AF-G81heK-nzLB91-G81hgP-JBqDKw-U87a83-ffmhci/), [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)